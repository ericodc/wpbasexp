<?php
//info: die if uninstall not called from Wordpress exit
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) )
	exit ();
if (is_multisite()) {
	delete_option('leafletmapsmarkerproinstaller_pluginupdatechecker');
	if ($blogs) {
		foreach($blogs as $blog) {
			switch_to_blog($blog['blog_id']);
			delete_option('leafletmapsmarkerproinstaller_pluginupdatechecker');
			}
		}
		restore_current_blog();
}
else
{
	delete_option('leafletmapsmarkerproinstaller_pluginupdatechecker');
}
?>
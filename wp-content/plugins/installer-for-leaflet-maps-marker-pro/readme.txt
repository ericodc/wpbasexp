=== Installer for Leaflet Maps Marker Pro ===
Contributors:      harmr
Plugin Name:       Installer for Leaflet Maps Marker Pro
Plugin URI:        http://www.mapsmarker.com
Tags:              installer, leaflet, maps
Author URI:        http://www.harm.co.at
Author:            Robert Harm
Donate link:       http://www.mapsmarker.com/donations
Requires at least: 2.x 
Tested up to:      3.6
Stable tag:        1.1

This plugin enables you to install the "Leaflet Maps Marker Pro" plugin package.

== Description ==

This plugin enables you to install the "Leaflet Maps Marker Pro" plugin package.

== Installation ==

= The Famous 3-Minute Installation =

1. Login on your WordPress site with your user account (needs to have admin rights!)
2. Select "Add New" from the "Plugins" menu
3. Click on "Upload"
4. Select file installer-for-leaflet-maps-marker-pro.zip from your local disc
5. Click on "Install now"
6. Click "Activate Plugin"

Done. You will immediately see an admin notice with a link which takes you to the installation page (also available via settings menu)

== Frequently Asked Questions ==

Q: Why does this Leaflet Maps Marker Pro need an installer in contrast to other plugins?
A: In order to check if your server meets the requirements before downloading the plugin.

Q: Does this plugin send and details about my server to you?
A: No, it just checks if your server meets the requirements for Leaflet Maps Marker Pro and offers you an option to start the download and installation of the pro plugin from mapsmarker.com.

== Screenshots ==

1. Admin notice with link to installation wizard page
2. Installation wizard page
3. Installation wizard page after pro plugin has been downloaded successfully

== Upgrade Notice ==
= v1.1 =
new minimum WordPress requirement 3.3; added new translations

== Changelog ==
= v1.1=
new minimum WordPress requirement 3.3
added new translations
= v1.0 =
initial release

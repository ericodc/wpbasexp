<?php
/*
    Leaflet Maps Marker Pro - Install 
*/
//info prevent file from being accessed directly
if (basename($_SERVER['SCRIPT_FILENAME']) == 'install-page.php') { die ("Please do not access this file directly. Thanks!<br/><a href='http://www.mapsmarker.com/go'>www.mapsmarker.com</a>"); }
$action = isset($_POST['action']) ? $_POST['action'] : '';
$lmm_pro_readme = WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'leaflet-maps-marker-pro' . DIRECTORY_SEPARATOR . 'readme.txt';
if (extension_loaded('ionCube Loader')) { if ( function_exists('ioncube_loader_iversion') ) { $ic_lv = ioncube_loader_iversion(); $lmm_ic_lv = (int)substr($ic_lv,0,1); } else { $ic_lv = ioncube_loader_version(); $lmm_ic_lv = (int)substr($ic_lv,0,1); } if ($lmm_ic_lv >= 4) { $sf = ''; } else { $sf = strrev('orp-'); } } else { $sf = strrev('orp-'); }
?>
<div class="wrap">

<table cellpadding="5" cellspacing="0" class="widefat fixed" style="margin-top:15px;">
	<tr>
	<td><div style="float:left;margin:2px 10px 0 0;"><a href="http://www.mapsmarker.com/go" target="_blank" title="www.mapsmarker.com"><img src="<?php echo LPI_PLUGIN_URL ?>inc/img/logo-mapsmarker-pro.png" width="65" height="65" alt="Leaflet Maps Marker Plugin Logo by Julia Loew, www.weiderand.net" /></a></div>
	<div style="font-size:2.5em;padding:8px 0 0 0;"><span style="font-weight:bold;">Maps Marker Pro<sup style="font-size:75%;">&reg;</sup> - <?php _e('installation wizard','lpi'); ?></span></div>
	</td></tr>
</table>

<?php
if ( $action == NULL ) {
	function lpi_php5() {
		if (version_compare(phpversion(),"5.2",">=")) {
			return true;
		} else {
			return false;
		}
	}
	function lpi_wp3() {
		global $wp_version;
		if (version_compare($wp_version,"3.3",">=")){
			return true;
		} else {
			return false;
		}
	}
	echo '<h2 style="margin-top:10px;">' . __('Checking requirements','lpi') . '</h2>';
	echo '<ul>';
	echo '<li><h3>' . sprintf(__('WordPress %s or greater','lpi'), '3.3') . ': ';
	if (lpi_wp3() === true) {
		echo '<span style="font-weight:bold;color:green;">OK</span></h3>';
	} else {
		global $wp_version;
		echo '<span style="font-weight:bold;color:red;">FAIL</span> - ' . __('your installed WordPress version:','lpi') . ' ' . $wp_version . '</h3>';
	}
	echo '</li>';
	echo '<li><h3>' . sprintf(__('PHP %s or greater','lpi'), '5.2') . ': ';
	if (lpi_php5() === true) {
		echo '<span style="font-weight:bold;color:green;">OK</span></h3>';
	} else {
		echo '<span style="font-weight:bold;color:red;">FAIL</span> - ' . __('your installed PHP version:','lpi') . ' ' . phpversion() . '</h3>';
	}
	echo '</li></ul><p>';
	
	if ( (lpi_wp3() === false) || (lpi_php5() === false) ) {
		echo '<h3 style="color:red;">' . __('Your server does NOT meets all requirements to install Leaflet Maps Marker Pro!','lpi') . '</h3><p>' . __('Please upgrade your server first as the components you are using are really outdated and might have security issues that have been solved with newer versions!','lpi') . '</p>';
	} else {
		echo '<h3>' . __('Your server meets all requirements to install Leaflet Maps Marker Pro!','lpi') . '</h3>';
		$dl_l = 'https://www.mapsmarker.com/download' . $sf;
		$dl_lt = 'www.mapsmarker.com/download' . $sf;
		echo '<p>' . sprintf(__('To start your free 30-day-trial of Leaflet Maps Marker Pro, please click on the button "start installation" below. This will start the download and installation of Leaflet Maps Marker Pro from <a style="text-decoration:none;" href="%1s">%2s</a>.<br/>Afterwards please activate the pro plugin and you will be guided through the process to receive a free 30-day-trial license without any obligations. Your trial will expire automatically unless you <a href="%3s" target="_blank">purchase a valid pro license</a>.','lpi'), $dl_l, $dl_lt, 'https://www.mapsmarker.com/store/order/index.php?task=product&product_id=1&category_id=1') . '</p>';
		echo '<form method="post"><input type="hidden" name="action" value="install_pro_version" />';
		wp_nonce_field('pro-install-nonce');
		if ( current_user_can( 'install_plugins' ) ) {
			echo '<input style="font-weight:bold;" type="submit" name="submit_install_pro_version" value="' . __('start installation','lpi') . ' &raquo;" class="submit button-primary" />';
		} else {
			echo '<div class="error" style="padding:10px;"><strong>' . sprintf(__('Warning: your user does not have the capability to install new plugins - please contact your administrator (%1s)','lpi'), '<a href="mailto:' . get_bloginfo('admin_email') . '?subject=' . esc_attr__('Please install the plugin "Leaflet MapsMarker Pro"','lpi') . '">' . get_bloginfo('admin_email') . '</a>' ) . '</strong></div>';
			echo '<input style="font-weight:bold;" type="submit" name="submit_upgrade_to_pro_version" value="' . __('start installation','lpi') . ' &raquo;" class="submit button-secondary" disabled="disabled" />';
		}
		echo '</form>';
	}
	echo '</p>';
} else {
	if (!wp_verify_nonce( $_POST['_wpnonce'], 'pro-install-nonce') ) { wp_die('<br/>'.__('Security check failed - please call this function from the according Leaflet Maps Marker admin page!','lpi').''); };
	if ($action == 'install_pro_version') {
		include_once ABSPATH . 'wp-admin/includes/class-wp-upgrader.php';
		add_filter( 'https_ssl_verify', '__return_false' ); //info: otherwise SSL error on localhost installs.
		add_filter( 'https_local_ssl_verify', '__return_false' ); //info: not sure if needed, added to be sure
		$upgrader = new Plugin_Upgrader( new Plugin_Upgrader_Skin() );
		$dl = 'https://www.mapsmarker.com/download' . $sf;
		$upgrader->install( $dl );
	} 
	$lmm_pro_readme = WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'leaflet-maps-marker-pro' . DIRECTORY_SEPARATOR . 'readme.txt';
	if (file_exists($lmm_pro_readme)) {
		echo '<p>' . __('Please activate the plugin by clicking the link above. The plugin "Installer for Leaflet Maps Marker Pro" will get deactivated automatically afterwards and can be deleted.','lpi') . '</p>';
	} else {
		$dl_l = 'https://www.mapsmarker.com/download' . $sf;
		$dl_lt = 'www.mapsmarker.com/download' . $sf;
		echo '<p>' . sprintf(__('The pro plugin package could not be downloaded automatically. Please download the plugin from <a href="%1s">%2s</a> and upload it to the directory /wp-content/plugins on your server manually','lpi'), $dl_l, $dl_lt) . '</p>';
	}
}
?>

<table cellpadding="5" cellspacing="0" style="margin-top:20px;border:1px solid #ccc;width:100%;background:#F9F9F9;">
  <tr>
    <td valign="center">
	<p style="margin:0px;">
			<a style="text-decoration:none;" href="http://www.mapsmarker.com" target="_blank"><img src="<?php echo LPI_PLUGIN_URL; ?>inc/img/icon-website-home.png" width="16" height="16" alt="mapsmarker.com"> MapsMarker.com</a>&nbsp;&nbsp;&nbsp;
			<a style="text-decoration:none;" href="http://www.mapsmarker.com/reviews" target="_blank" title="<?php esc_attr_e('please review this plugin on wordpress.org','lpi') ?>"><img src="<?php echo LPI_PLUGIN_URL; ?>inc/img/icon-star.png" width="16" height="16" alt="ratings"> <?php _e('Rate plugin','lpi') ?></a>&nbsp;&nbsp;&nbsp;
			<a style="text-decoration:none;" href="http://translate.mapsmarker.com/projects/lmm" target="_blank"><img src="<?php echo LPI_PLUGIN_URL; ?>inc/img/icon-translations.png" width="16" height="16" alt="translations"> <?php echo __('translations','lpi'); ?></a>&nbsp;&nbsp;&nbsp;
			<a style="text-decoration:none;" href="http://twitter.com/mapsmarker" target="_blank"><img src="<?php echo LPI_PLUGIN_URL; ?>inc/img/icon-twitter.png" width="16" height="16" alt="twitter"> Twitter</a>&nbsp;&nbsp;&nbsp;
			<a style="text-decoration:none;" href="http://facebook.com/mapsmarker" target="_blank"><img src="<?php echo LPI_PLUGIN_URL; ?>inc/img/icon-facebook.png" width="16" height="16" alt="facebook"> Facebook</a>&nbsp;&nbsp;&nbsp;
			<a style="text-decoration:none;" href="http://www.mapsmarker.com/+" target="_blank"><img src="<?php echo LPI_PLUGIN_URL; ?>inc/img/icon-google-plus.png" width="16" height="16" alt="google+"> Google+</a>&nbsp;&nbsp;&nbsp;
			<a style="text-decoration:none;" href="http://www.mapsmarker.com/changelog" target="_blank"><img src="<?php echo LPI_PLUGIN_URL; ?>inc/img/icon-changelog-header.png" width="16" height="16" alt="changelog"> <?php _e('Changelog','lpi') ?></a>&nbsp;&nbsp;&nbsp;
			<a style="text-decoration:none;" href="http://feeds.feedburner.com/MapsMarker" target="_blank"><img src="<?php echo LPI_PLUGIN_URL; ?>inc/img/icon-rss.png" width="16" height="16" alt="rss"> RSS</a>&nbsp;&nbsp;&nbsp;
			<a style="text-decoration:none;" href="http://feedburner.google.com/fb/a/mailverify?uri=MapsMarker" target="_blank"><img src="<?php echo LPI_PLUGIN_URL; ?>inc/img/icon-rss-email.png" width="16" height="16" alt="rss-email"> <?php echo __('RSS via E-Mail','lpi'); ?></a>&nbsp;&nbsp;&nbsp;
			</p></td>
  </tr>
</table>

msgid ""
msgstr ""
"Project-Id-Version: Leaflet Maps Marker Installer\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-08-16 19:57+0100\n"
"PO-Revision-Date: 2013-08-16 19:58+0100\n"
"Last-Translator: Robert Harm <info@mapsmarker.com>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: __;gettext;gettext_noop;_e;esc_attr__;esc_attr_e\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-Language: English\n"
"X-Poedit-Country: AUSTRIA\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../install-page.php:16
msgid "installation wizard"
msgstr ""

#: ../install-page.php:37
msgid "Checking requirements"
msgstr ""

#: ../install-page.php:39
#, php-format
msgid "WordPress %s or greater"
msgstr ""

#: ../install-page.php:44
msgid "your installed WordPress version:"
msgstr ""

#: ../install-page.php:47
#, php-format
msgid "PHP %s or greater"
msgstr ""

#: ../install-page.php:51
msgid "your installed PHP version:"
msgstr ""

#: ../install-page.php:56
msgid "Your server does NOT meets all requirements to install Leaflet Maps Marker Pro!"
msgstr ""

#: ../install-page.php:56
msgid "Please upgrade your server first as the components you are using are really outdated and might have security issues that have been solved with newer versions!"
msgstr ""

#: ../install-page.php:58
msgid "Your server meets all requirements to install Leaflet Maps Marker Pro!"
msgstr ""

#: ../install-page.php:61
#, php-format
msgid "To start your free 30-day-trial of Leaflet Maps Marker Pro, please click on the button \"start installation\" below. This will start the download and installation of Leaflet Maps Marker Pro from <a style=\"text-decoration:none;\" href=\"%1s\">%2s</a>.<br/>Afterwards please activate the pro plugin and you will be guided through the process to receive a free 30-day-trial license without any obligations. Your trial will expire automatically unless you <a href=\"%3s\" target=\"_blank\">purchase a valid pro license</a>."
msgstr ""

#: ../install-page.php:65
#: ../install-page.php:68
msgid "start installation"
msgstr ""

#: ../install-page.php:67
#, php-format
msgid "Warning: your user does not have the capability to install new plugins - please contact your administrator (%1s)"
msgstr ""

#: ../install-page.php:67
msgid "Please install the plugin \"Leaflet MapsMarker Pro\""
msgstr ""

#: ../install-page.php:74
msgid "Security check failed - please call this function from the according Leaflet Maps Marker admin page!"
msgstr ""

#: ../install-page.php:85
msgid "Please activate the plugin by clicking the link above. The plugin \"Installer for Leaflet Maps Marker Pro\" will get deactivated automatically afterwards and can be deleted."
msgstr ""

#: ../install-page.php:89
#, php-format
msgid "The pro plugin package could not be downloaded automatically. Please download the plugin from <a href=\"%1s\">%2s</a> and upload it to the directory /wp-content/plugins on your server manually"
msgstr ""

#: ../install-page.php:99
msgid "please review this plugin on wordpress.org"
msgstr ""

#: ../install-page.php:99
msgid "Rate plugin"
msgstr ""

#: ../install-page.php:100
msgid "translations"
msgstr ""

#: ../install-page.php:104
msgid "Changelog"
msgstr ""

#: ../install-page.php:106
msgid "RSS via E-Mail"
msgstr ""

#: ../installer-for-leaflet-maps-marker-pro.php:30
msgid "Your are using WordPress Multisite and the plugin \"Leaflet Maps Marker Pro\" has already been activated networkwide or on another subsite only."
msgstr ""

#: ../installer-for-leaflet-maps-marker-pro.php:30
#, php-format
msgid "If you do not see the menu entry \"Maps Marker Pro\", please contact your network admin (%1$s) for more details."
msgstr ""

#: ../installer-for-leaflet-maps-marker-pro.php:30
#: ../installer-for-leaflet-maps-marker-pro.php:77
msgid "Please activate the plugin \"Leaflet Maps Marker Pro\""
msgstr ""

#: ../installer-for-leaflet-maps-marker-pro.php:68
msgid "Please click here to start the installation of Leaflet Maps Marker Pro"
msgstr ""

#: ../installer-for-leaflet-maps-marker-pro.php:75
#: ../installer-for-leaflet-maps-marker-pro.php:77
msgid "The plugin \"Leaflet Maps Marker Pro\" has already been copied to your server but not yet been activated."
msgstr ""

#: ../installer-for-leaflet-maps-marker-pro.php:75
#, php-format
msgid "Please navigate to <a href=\"%1$s\">Plugins / Installed Plugins</a> and activate the plugin \"Leaflet Maps Marker Pro\"."
msgstr ""

#: ../installer-for-leaflet-maps-marker-pro.php:77
#, php-format
msgid "Please contact your administrator (%1s) to activate the plugin \"Leaflet Maps Marker Pro\"."
msgstr ""


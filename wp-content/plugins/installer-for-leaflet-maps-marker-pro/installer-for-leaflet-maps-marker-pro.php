<?php
/*
Plugin Name: Installer for Leaflet Maps Marker Pro
Version: 1.1
Plugin URI: http://www.mapsmarker.com
Description: This plugin enables you to install the "Leaflet Maps Marker Pro" plugin package.
Author: Robert Harm
Author URI: http://www.harm.co.at
*/
//info prevent file from being accessed directly
if (basename($_SERVER['SCRIPT_FILENAME']) == 'installer-for-leaflet-maps-marker.php') { die ("Please do not access this file directly. Thanks!"); }
if ( ! defined( 'LPI_PLUGIN_URL' ) )
	define ("LPI_PLUGIN_URL", plugin_dir_url(__FILE__));
if ( ! defined( 'LPI_WP_ADMIN_URL' ) )
	define( 'LPI_WP_ADMIN_URL', get_admin_url() );

if ( is_admin() ) {
	require_once( plugin_dir_path( __FILE__ ) . 'inc' . DIRECTORY_SEPARATOR . 'class-plugin-update-checker-installer.php' );
}

//info: deactive free version first if active
include_once( ABSPATH . 'wp-admin' . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'plugin.php' );
if (is_plugin_active('leaflet-maps-marker-pro/leaflet-maps-marker.php') ) {
	if (!is_multisite()) {
		deactivate_plugins('installer-for-leaflet-maps-marker-pro/installer-for-leaflet-maps-marker-pro.php', $silent = false, $network_wide = null);
	} else {
		include_once( ABSPATH . 'wp-admin' . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'plugin.php' );
		if (is_plugin_active('leaflet-maps-marker-pro/leaflet-maps-marker.php') ) {
			function lpi_admin_notices_multisite() {
				echo '<p><div class="updated" style="padding:5px;"><table><tr><td><img src="' . LPI_PLUGIN_URL . 'inc/img/logo-mapsmarker-pro.png" width="50" height="50" /></td><td>' . __('Your are using WordPress Multisite and the plugin "Leaflet Maps Marker Pro" has already been activated networkwide or on another subsite only.','lpi') . '<br/>' . sprintf(__('If you do not see the menu entry "Maps Marker Pro", please contact your network admin (%1$s) for more details.','lpi'), '<a href="mailto:' . get_bloginfo('admin_email') . '?subject=' . esc_attr__('Please activate the plugin "Leaflet Maps Marker Pro"','lpi') . '">' . get_bloginfo('admin_email') . '</a>') . '</td></tr></table></div></p>';
			}
			add_action('admin_notices', 'lpi_admin_notices_multisite',3);
		}
		include_once( ABSPATH . 'wp-includes' . DIRECTORY_SEPARATOR . 'pluggable.php' );
		if (is_network_admin()) {
			deactivate_plugins('installer-for-leaflet-maps-marker-pro/installer-for-leaflet-maps-marker-pro.php', $silent = false, $network_wide = true);
		} else {
			deactivate_plugins('installer-for-leaflet-maps-marker-pro/installer-for-leaflet-maps-marker-pro.php', $silent = false, $network_wide = null);
		}
	}
}

class LeafletMapsMarkerProInstaller
{
	function __construct() {
		add_action('init', array(&$this, 'lpi_load_translation_files'),1);
		add_action('admin_menu', array(&$this, 'lpi_admin_menu'),2);
		add_action('admin_notices', array(&$this, 'lpi_admin_notices'),3);
	}
	function lpi_load_translation_files() {
		load_plugin_textdomain('lpi', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/');
	}
	function lpi_admin_menu() {
		include_once( ABSPATH . 'wp-admin' . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'plugin.php' );
		//info: needed for subsites on multisite installation if pro plugin is activated network wide
		if (!is_plugin_active('leaflet-maps-marker-pro/leaflet-maps-marker.php') ) {
			$settings_page = add_object_page('Installer for Leaflet Maps Marker Pro', 'Maps Marker Pro', 'activate_plugins', 'leafletmapsmarkerpro_installer', array(&$this, 'lpi_display_install_page'), LPI_PLUGIN_URL . 'inc/img/icon-menu-page.png' );
		}
	}
	function lpi_display_install_page() {
		include('install-page.php');
	}	
	function lpi_admin_notices() {
		$lmm_pro_readme = WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'leaflet-maps-marker-pro' . DIRECTORY_SEPARATOR . 'readme.txt';
		if (!file_exists($lmm_pro_readme)) {
			$lpi_current_page = isset($_GET['page']) ? $_GET['page'] : '';
			if (! in_array($lpi_current_page, array('leafletmapsmarkerpro_installer','leafletmapsmarker_license'))) {		
				echo '<p><div class="updated" style="padding:5px;"><table><tr><td><a href="' . LPI_WP_ADMIN_URL . 'admin.php?page=leafletmapsmarkerpro_installer"><img src="' . LPI_PLUGIN_URL . 'inc/img/logo-mapsmarker-pro.png" width="50" height="50" /></a></td><td><a href="' . LPI_WP_ADMIN_URL . 'admin.php?page=leafletmapsmarkerpro_installer"">' . __('Please click here to start the installation of Leaflet Maps Marker Pro','lpi') . '</td></tr></table></div></p>';
			}
		} else {
			//info: show info if pro plugin is on server but inactive
			include_once( ABSPATH . 'wp-admin' . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'plugin.php' );
			if (!is_plugin_active('leaflet-maps-marker-pro/leaflet-maps-marker.php') ) {
				if ( current_user_can( 'install_plugins' ) ) {
					echo '<p><div class="updated" style="padding:5px;"><table><tr><td><a href="' . LPI_WP_ADMIN_URL . 'admin.php?page=leafletmapsmarkerpro_installer"><img src="' . LPI_PLUGIN_URL . 'inc/img/logo-mapsmarker-pro.png" width="50" height="50" /></a></td><td><a href="' . LPI_WP_ADMIN_URL . 'admin.php?page=leafletmapsmarkerpro_installer"">' . __('The plugin "Leaflet Maps Marker Pro" has already been copied to your server but not yet been activated.','lpi') . '<br/>' . sprintf(__('Please navigate to <a href="%1$s">Plugins / Installed Plugins</a> and activate the plugin "Leaflet Maps Marker Pro".','lpi'), LPI_WP_ADMIN_URL . 'plugins.php') . '</td></tr></table></div></p>';
				} else {
					echo '<p><div class="updated" style="padding:5px;"><table><tr><td><a href="' . LPI_WP_ADMIN_URL . 'admin.php?page=leafletmapsmarkerpro_installer"><img src="' . LPI_PLUGIN_URL . 'inc/img/logo-mapsmarker-pro.png" width="50" height="50" /></a></td><td><a href="' . LPI_WP_ADMIN_URL . 'admin.php?page=leafletmapsmarkerpro_installer"">' . __('The plugin "Leaflet Maps Marker Pro" has already been copied to your server but not yet been activated.','lpi') . '<br/>' . sprintf(__('Please contact your administrator (%1s) to activate the plugin "Leaflet Maps Marker Pro".','lpi'), '<a href="mailto:' . get_bloginfo('admin_email') . '?subject=' . esc_attr__('Please activate the plugin "Leaflet Maps Marker Pro"','lpi') . '">' . get_bloginfo('admin_email') . '</a>' ) . '</td></tr></table></div></p>';
				}			
			} else {
				//info: nothing to to - plugin already got deactivated if this is true	
			}
		}
	}
}  //info: end class

if ( is_admin() ) {
	$run_PluginUpdateCheckerInstaller = new PluginUpdateChecker(
		'http://www.mapsmarker.com/updates/?action=get_metadata&slug=installer-for-leaflet-maps-marker-pro',
		'installer-for-leaflet-maps-marker-pro/installer-for-leaflet-maps-marker-pro.php',
		'installer-for-leaflet-maps-marker-pro',
		'168',
		'leafletmapsmarkerproinstaller_pluginupdatechecker'
	);
}
$run_leafletmapsmarkerproinstaller = new LeafletMapsMarkerProInstaller();
unset($run_leafletmapsmarkerproinstaller);
unset($run_PluginUpdateCheckerInstaller);
?>
